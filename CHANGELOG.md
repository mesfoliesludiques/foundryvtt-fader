# Changelog
All notable changes to this project will be documented in this file.

## [0.0.2] - 2021-01-20
### Added
- Fade control blinks in red on fadeout
### Changed
### Removed

## [0.0.1] - 2021-01-16
### Added
- Fade control for individual sounds
### Changed
### Removed
