Hooks.on("renderPlaylistDirectory", (playlist, html, data) => {
    const controls = html.find(".sound-controls")
    controls.each((k) => {
        // Add fader control
        let fader = document.createElement("a");
        fader.classList.add("sound-control");
        fader.setAttribute("data-action", "sound-fade");
        fader.setAttribute("title", game.i18n.localize("FADER.Fade"));
        fader.innerText = "F";
        controls[k].appendChild(fader);
        fader.addEventListener("click", SoundFade)
    });
});

function SoundFade(event) {
    const playlistId = event.currentTarget.parentElement.parentElement.parentElement.parentElement.dataset["entityId"];
    const soundId = event.currentTarget.parentElement.parentElement.dataset["soundId"];
    const playlist = game.playlists.get(playlistId);
    const howlSound = playlist.audio[soundId].howl;
    const sound = playlist.sounds.filter(i => i._id === soundId)[0];
    const vol = sound.volume * game.settings.get("core", "globalPlaylistVolume");
    if (sound.playing) {
        game.socket.emit("module.fader", { soundId: soundId, playlistId: playlistId, fadeOut: true })
        howlSound.fade(vol, 0, 3000);
        event.currentTarget.classList.add("fadeout");
        howlSound.on("fade", () => {
            if (howlSound._volume === 0) {
                const data = { _id: sound._id, playing: false };
                playlist.updateEmbeddedEntity("PlaylistSound", data, {});
            }
        }, howlSound._sounds[0]._id)
    } else {
        const data = { _id: sound._id, playing: true };
        playlist.updateEmbeddedEntity("PlaylistSound", data, {}).then( () => {
            game.socket.emit("module.fader", { soundId: soundId, playlistId: playlistId, fadeIn: true })
            howlSound.fade(0, vol, 3000);
        });
    }
}

Hooks.once("init", () => {
    // Listen to the socket
    game.socket.on("module.fader", (data) => {
        const playlist = game.playlists.get(data.playlistId);
        const howlSound = playlist.audio[data.soundId].howl;
        const sound = playlist.sounds.filter(i => i._id === data.soundId)[0];
        const vol = sound.volume * game.settings.get("core", "globalPlaylistVolume");
        if (data.fadeOut) {
            howlSound.fade(vol, 0, 3000);
        } else if (data.fadeIn) {
            howlSound.fade(0, vol, 3000);
        }
    });
})